<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="qtqr.py" line="35"/>
        <source>QtQR: QR Code Generator</source>
        <translation>QtQR：QR 二维码生成器</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="46"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="47"/>
        <source>URL</source>
        <translation>URL（超链接）</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="48"/>
        <source>Bookmark</source>
        <translation>书签</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="49"/>
        <source>E-Mail</source>
        <translation>电子邮箱</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="50"/>
        <source>Telephone Number</source>
        <translation>电话号码</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="51"/>
        <source>Contact Information (PhoneBook)</source>
        <translation>联系人信息（手机电话簿）</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="52"/>
        <source>SMS</source>
        <translation>短信</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="53"/>
        <source>MMS</source>
        <translation>彩信</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="54"/>
        <source>Geolocalization</source>
        <translation>地理位置信息</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="55"/>
        <source>WiFi Network</source>
        <translation>WiFi 网络</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="99"/>
        <source>Text to be encoded:</source>
        <translation>要编码的文本：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="103"/>
        <source>URL to be encoded:</source>
        <translation>要编码的 URL：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="107"/>
        <source>Title:</source>
        <translation>标题：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="139"/>
        <source>URL:</source>
        <translation>URL：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="113"/>
        <source>E-Mail address:</source>
        <translation>电子邮箱地址：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="115"/>
        <source>Subject:</source>
        <translation>主题：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="117"/>
        <source>Message Body:</source>
        <translation>信息内容：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="150"/>
        <source>Telephone Number:</source>
        <translation>电话号码：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="125"/>
        <source>Name:</source>
        <translation>名字：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="127"/>
        <source>Telephone:</source>
        <translation>电话：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="129"/>
        <source>E-Mail:</source>
        <translation>电子邮箱：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="131"/>
        <source>Note:</source>
        <translation>注释：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="133"/>
        <source>Birthday:</source>
        <translation>生日：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="136"/>
        <source>Address:</source>
        <translation>地址：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="138"/>
        <source>Insert separated by commas the PO Box, room number, house number, city, prefecture, zip code and country in order</source>
        <translation>输入内容用逗号分割，按照顺序输入邮箱号码、房间号码、房屋号码、城市、行政区域、邮政编号和国家地区信息</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="145"/>
        <source>Message:</source>
        <translation>信息：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="147"/>
        <source>characters count: 0</source>
        <translation>字符计数：0</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="152"/>
        <source>Content:</source>
        <translation>内容：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="156"/>
        <source>Latitude:</source>
        <translation>纬度：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="158"/>
        <source>Longitude:</source>
        <translation>经度：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="162"/>
        <source>SSID:</source>
        <translation>SSID：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="164"/>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="166"/>
        <source>Show Password</source>
        <translation>显示密码</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="167"/>
        <source>Encription:</source>
        <translation>加密方式：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WEP</source>
        <translation>WEP</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>WPA/WPA2</source>
        <translation>WPA/WPA2</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="169"/>
        <source>No Encription</source>
        <translation>无加密</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="179"/>
        <source>Parameters:</source>
        <translation>参数：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="181"/>
        <source>&amp;Pixel Size:</source>
        <translation>像素大小(&amp;P)：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="184"/>
        <source>&amp;Error Correction:</source>
        <translation>错误纠正(&amp;E)：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Lowest</source>
        <translation>最低</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Medium</source>
        <translation>中等</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>QuiteGood</source>
        <translation>较好</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="186"/>
        <source>Highest</source>
        <translation>最高</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="195"/>
        <source>&amp;Margin Size:</source>
        <translation>边缘大小(&amp;M)：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="199"/>
        <source>Start typing to create QR Code
 or  drop here image files for decoding.</source>
        <translation>开始输入以生成 QR 二维码
或把图片拖放到这里以解码。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="206"/>
        <source>&amp;Save QRCode</source>
        <translation>保存二维码(&amp;S)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="207"/>
        <source>&amp;Decode</source>
        <translation>解码(&amp;D)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="210"/>
        <source>Decode from &amp;File</source>
        <translation>从文件解码(&amp;F)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="211"/>
        <source>Decode from &amp;Webcam</source>
        <translation>从摄像头解码(&amp;W)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="214"/>
        <source>E&amp;xit</source>
        <translation>离开(&amp;X)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="216"/>
        <source>&amp;About</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="229"/>
        <source>Error Correction Level</source>
        <translation>错误纠正等级</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="366"/>
        <source>Select data type:</source>
        <translation>选择数据类别：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="402"/>
        <source>characters count: %s - %d message(s)</source>
        <translation>字符计数：%s - %d 个信息</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="486"/>
        <source>ERROR: Something went wrong while trying to generate the QR Code.</source>
        <translation>错误：生成 QR 二维码时出现问题。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>Save QRCode</source>
        <translation>保存二维码</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>Save QR Code</source>
        <translation>保存 QR 二维码</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="522"/>
        <source>QR Code succesfully saved to %s</source>
        <translation>QR 二维码成功保存至 %s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="529"/>
        <source>QRCode succesfully saved to &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>QR 码成功保存至 &lt;b&gt;%s&lt;/b&gt;。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Open QRCode</source>
        <translation>打开 QR 二维码</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="537"/>
        <source>Images (*.png *.jpg);; All Files (*.*)</source>
        <translation>图像 (*.png *.jpg);; 所有文件 (*.*)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>Decode File</source>
        <translation>解码文件</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="548"/>
        <source>No QRCode could be found in file: &lt;b&gt;%s&lt;/b&gt;.</source>
        <translation>文件 &lt;b&gt;%s&lt;/b&gt; 中未找到二维码。</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="578"/>
        <source>QRCode contains the following text:

%s</source>
        <translation>二维码包含了如下的文本：

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="579"/>
        <source>QRCode contains the following url address:

%s</source>
        <translation>二维码包含了如下的 url 地址：

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="580"/>
        <source>QRCode contains a bookmark:

Title: %s
URL: %s</source>
        <translation>二维码包含了一个书签：

标题：%s
URL：%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="581"/>
        <source>QRCode contains the following e-mail address:

%s</source>
        <translation>二维码包含下列电子邮件地址：

%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="582"/>
        <source>QRCode contains an e-mail message:

To: %s
Subject: %s
Message: %s</source>
        <translation>二维码包含了一封电子邮件信息：

收件人：%s
主题：%s
信息：%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="583"/>
        <source>QRCode contains a telephone number: </source>
        <translation>二维码包含了一个电话号码：</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="584"/>
        <source>QRCode contains a phonebook entry:

Name: %s
Tel: %s
E-Mail: %s
Note: %s
Birthday: %s
Address: %s
URL: %s</source>
        <translation>二维码包含了一个通讯录条目：

名称：%s
电话：%s
电子邮件：%s
注释：%s
生日：%s
地址：%s
URL：%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="592"/>
        <source>QRCode contains the following SMS message:

To: %s
Message: %s</source>
        <translation>二维码包含了下列的短信信息：

收信人：%s
信息：%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="593"/>
        <source>QRCode contains the following MMS message:

To: %s
Message: %s</source>
        <translation>二维码包含了下列的彩信信息：

收信人：%s
信息：%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="594"/>
        <source>QRCode contains the following coordinates:

Latitude: %s
Longitude:%s</source>
        <translation>二维码包含了下列的坐标：

纬度：%s
经度：%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="595"/>
        <source>QRCode contains the following WiFi Network Configuration:

SSID: %s
Encription Type: %s
Password: %s</source>
        <translation>二维码包含了下列 WiFi 网络配置：

SSID：%s
加密方式：%s
密码：%s</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="597"/>
        <source>

Do you want to </source>
        <translation>

您是否想要 </translation>
    </message>
    <message>
        <location filename="qtqr.py" line="601"/>
        <source>open it in a browser?</source>
        <translation>在浏览器中打开它？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="602"/>
        <source>send an e-mail to the address?</source>
        <translation>向该地址发送电子邮件？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="603"/>
        <source>send the e-mail?</source>
        <translation>发送电子邮件？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="608"/>
        <source>open it in Google Maps?</source>
        <translation>在谷歌地图上打开？</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="625"/>
        <source>Decode QRCode</source>
        <translation>对二维码解码</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="632"/>
        <source>&amp;Edit</source>
        <translation>编辑(&amp;E)</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>Decoding Failed</source>
        <translation>解码失败</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="717"/>
        <source>&lt;p&gt;Oops! no code was found.&lt;br /&gt; Maybe your webcam didn&apos;t focus.&lt;/p&gt;</source>
        <translation>&lt;p&gt;糟糕！没找到二维码。&lt;br /&gt;也许您的摄像头没正常对焦。&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>About QtQR</source>
        <translation>关于 QtQR</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="728"/>
        <source>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;A simple software for creating and decoding QR Codes that uses &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; as backend. Both are part of the &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; project.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;This is Free Software: GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Please visit our website for more information and to check out the code:&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;copyright &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</source>
        <translation>&lt;h1&gt;QtQR %s&lt;/h1&gt;&lt;p&gt;一个创建和解码 QR 二维码的简单软件，使用了 &lt;a href=&quot;https://code.launchpad.net/~qr-tools-developers/qr-tools/python-qrtools-trunk&quot;&gt;python-qrtools&lt;/a&gt; 作为后端。两者都是 &lt;a href=&quot;https://launchpad.net/qr-tools&quot;&gt;QR Tools&lt;/a&gt; 项目的一部分。&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;本软件是自由软件：使用 GNU-GPLv3&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;请访问我们的网站以了解更多信息并检查我们的源代码：&lt;br /&gt;&lt;a href=&quot;https://launchpad.net/~qr-tools-developers/qtqr&quot;&gt;https://launchpad.net/~qr-tools-developers/qtqr&lt;/p&gt;&lt;p&gt;版权所有 &amp;copy; Ramiro Algozino &amp;lt;&lt;a href=&quot;mailto:algozino@gmail.com&quot;&gt;algozino@gmail.com&lt;/a&gt;&amp;gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>VideoDevices</name>
    <message>
        <location filename="qtqr.py" line="771"/>
        <source>Decode from Webcam</source>
        <translation>从摄像头解码</translation>
    </message>
    <message>
        <location filename="qtqr.py" line="777"/>
        <source>You are about to decode from your webcam. Please put the code in front of your camera with a good light source and keep it steady.
Once you see a green rectangle you can close the window by pressing any key.

Please select the video device you want to use for decoding:</source>
        <translation>您即将从您的摄像头获取图片进行解码。请将您的二维码放在您的摄像机前，保证良好的光照并保持其稳定。
一旦您看见一个绿色矩形框，请按任意一个键以关闭窗口。

请选择您想要用于解码的视频设备：</translation>
    </message>
</context>
</TS>
